﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiiviharjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            while(true)
            {
                Console.WriteLine("Anna õpilase nimi: ");//nummerdamine pole vajalik, i+1 on selle pärast et ta nullist ei alustaks
                string nimi = Console.ReadLine();
                //kui tühi vastus
                if (nimi == "") break;
                Console.WriteLine($"Anna õpilase {nimi} vanus: ");
                int vanus = int.Parse(Console.ReadLine());
                nimekiri.Add(nimi, vanus);
            }

            Console.WriteLine("Keskmine vanus on {0}", nimekiri.Values.Average());
            int maxVanus = nimekiri.Values.Max();
            foreach(var x in nimekiri)
                if(x.Value==maxVanus)
                    Console.WriteLine($"Vanim on {x.Key}, ta on {x.Value} aastat vana");
            int minVanus = nimekiri.Values.Min();
            foreach (var x in nimekiri)
                if (x.Value == minVanus)
                    Console.WriteLine($"Noorim on {x.Key}, ta on {x.Value} aastat vana");
        }
    }
}
